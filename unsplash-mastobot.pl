#!/usr/bin/perl
use strict;
use warnings;
use 5.10.0;

use POSIX;
use Image::Randim::Source;
use Image::Magick;
use Mastodon::Client;
use Mojo::UserAgent;
use Mojo::File qw(tempfile);
use Mojo::Util qw(encode);

my $file = tempfile(DIR => '/tmp');

say 'Asking for random image';
my $source = Image::Randim::Source->new;
$source->set_provider('Unsplash');
my $image = $source->get_image;

say 'Fetching image: ', $image->url;

my $ua  = Mojo::UserAgent->new;
my $res = $ua->get($image->url)->result;

if ($res->is_success) {
    say 'Image fetched and saved as ', $file->to_abs;
    $file->spurt($res->body);
} elsif ($res->is_error) {
    say $res->message;
    exit;
}

my $size = -s $file->to_abs;
say 'Image size: ', $size;
while ($size > 4 * 1024 * 1024) {
    my $im = Image::Magick->new;
    $im->BlobToImage($file->slurp);

    my $x = floor($im->Get('width') * 0.9);
    say 'Reducing image ', $x;
    $im->Resize(geometry => $x);

    $file->spurt($im->ImageToBlob());
    $size = -s $file->to_abs;
    say 'New image size: ', $size;
}

my $client = Mastodon::Client->new(
    instance        => $ENV{HOST},
    name            => 'Unsplash mastobot',
    website         => 'https://framagit.org/luc/unsplash-mastobot',
    scopes          => ['write'],
    client_id       => $ENV{CLIENT_ID},
    client_secret   => $ENV{CLIENT_SECRET},
    access_token    => $ENV{ACCESS_TOKEN},
    coerce_entities => 0
);

say 'Uploading image';
my $media = $client->upload_media($file->to_abs);

say 'Posting status';
$client->post_status(sprintf("Shot by %s\n%s", encode('UTF-8', $image->owner_name), $image->link),
    {
        media_ids  => [$media->{id}],
        visibility => 'public'
    }
);

say 'Deleting temporary file';
unlink $file->to_abs;

say 'All has been well. Work ended.';
